#!/usr/bin/env sh

: ${INFO:="[\e[92mINFO\e[0m]"}; 						export INFO;
: ${ERROR:="[\e[91mERROR\e[0m]"};						export ERROR;
: ${WARNING:="[\e[93mWARNING\e[0m]"};					export WARNING;

# Exit immediately on error
set -e

    if [ -z ${VAULT_ADDR+x} ]; then
        echo -e "${ERROR} VAULT_ADDR is required and appears to be missing"
        exit 1;
    fi

    if [ -z ${VAULT_PORT+x} ]; then
        echo -e "${ERROR} VAULT_PORT is required and appears to be missing"
        exit 1;
    fi

    if [ -z ${CLIENT_DOMAIN+x} ]; then
        echo -e "${ERROR} VAULT_PORT is required and appears to be missing"
        exit 1;
    fi

: ${VAULT_ROLE:=${CLIENT_DOMAIN}}; 						export VAULT_ROLE;
: ${VAULT_PATH:=/v1/secret/${CLIENT_DOMAIN}/shared}; 	export VAULT_PATH;


# If we are on an ec2 instance this will get a token by verifying the ec2 instance
vault_ec2_login() {
	PKCS7_KEY=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/pkcs7 | tr -d '\n')
	NONCE=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

	if [ -n "$PKCS7_KEY" ]
	then
		echo -e "${INFO} Getting EC2 Token for ${CLIENT_DOMAIN}"

		VAULT_LOGIN=$(curl \
			-X POST "${VAULT_ADDR}:${VAULT_PORT}/v1/auth/aws-ec2/login" \
			-d '{"role":"'${VAULT_ROLE}'","pkcs7":"'${PKCS7_KEY}'","nonce":"'${NONCE}'"}' \
			-s)

		ERRORS=$(echo ${VAULT_LOGIN} | jq -r .errors[0])
		if [ "$ERRORS" != "null" ]; then
			echo -e "${ERROR}: $ERRORS"
		fi


		export VAULT_TOKEN=$(echo ${VAULT_LOGIN} | jq -r '.auth.client_token')

		if [ "$VAULT_TOKEN" == "null" ]; then
			echo -e "${ERROR} Token is null"
			exit 1;
		fi
	fi
}

# Check if the vault is unsealed.
is_unsealed() {
    IS_SEALED=$(curl -s ${VAULT_ADDR}:${VAULT_PORT}/v1/sys/seal-status | jq '.sealed' )
    [ "$IS_SEALED" == "false" ]
}

# Get the environment variables out of Vault.
has_config() {
    VAULT_CONFIG_JSON=$(curl \
			-H "X-Vault-Token: ${VAULT_TOKEN}" \
			-H "Content-Type: application/json" \
			-s \
			-X GET ${VAULT_ADDR}:${VAULT_PORT}${VAULT_PATH})

			CONFIG_ERRORS=$(echo ${VAULT_CONFIG_JSON} | jq -r .errors[0])
			if [ "$CONFIG_ERRORS" != "null" ]; then
				echo -e "${ERROR} Configuration: ${CONFIG_ERRORS}"
			fi

			[ "$CONFIG_ERRORS" == "null" ]
}

>&2 echo -e "${INFO} Vault login"
if [ -z ${VAULT_TOKEN+x} ]; then
		vault_ec2_login
	else
		echo -e "${INFO} Using override vault token for ${CLIENT_DOMAIN}"
fi

until is_unsealed; do
  >&2 echo -e "${WARNING} Vault is still sealed - sleeping"
  sleep 30
done

until has_config; do
  >&2 echo -e "${WARNING} Configuration is not yet available - sleeping"
  sleep 60
done

export VAULT_CONFIG_JSON;

#for backwards compatibility
export JSON=VAULT_CONFIG_JSON;