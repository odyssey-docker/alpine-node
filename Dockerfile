FROM mhart/alpine-node:6.5.0

LABEL maintainer "Greg Keys <gkeys@mumba.cloud>"

COPY ./scripts/wait-for-it.sh /usr/local/bin/wait-for-it
COPY ./scripts/vault_bootstrap.sh /usr/local/bin/vault_bootstrap

RUN chmod +x /usr/local/bin/wait-for-it /usr/local/bin/vault_bootstrap